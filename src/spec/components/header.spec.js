import React from 'react'
import { shallow } from 'enzyme'
import Header from '../../components/header'
import { NavLink } from 'react-router-dom'
import { createMemoryHistory } from 'history'

it('renders correctly', () => {
  let wrapper = shallow(<Header />)
  expect(wrapper.find('header')).toHaveLength(1)
  expect(wrapper.find('img.header-logo')).toHaveLength(1)
  expect(wrapper.find('a')).toHaveLength(1)
  expect(wrapper.find(NavLink)).toHaveLength(1)
})
