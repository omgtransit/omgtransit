import React from 'react'
import { shallow } from 'enzyme'
import App from '../../components/app'
import Header from '../../components/header'
import StatusBar from '../../components/status_bar'
import Footer from '../../components/footer'
import moment from 'moment'

let initialProps = {
  bikeStations: { isLoading: true },
  userLocation: { isLoading: true }
}

it('renders without crashing', () => {
  let wrapper = shallow(<App />)
  expect(wrapper.find('div')).toHaveLength(2)
  expect(wrapper.find(Header)).toHaveLength(1)
  // we don't expect to see StatusBar here because it returns null
  expect(wrapper.find(StatusBar)).toHaveLength(0)
  expect(wrapper.find(Footer)).toHaveLength(1)
})

it('renders children when passed in', () => {
  let child = <div className='child'>I am a child</div>
  let wrapper = shallow(<App>{child}</App>)
  expect(wrapper.find('div')).toHaveLength(3)
  expect(wrapper.find('div.child')).toHaveText('I am a child')
})

it('updates user location on mount if coords are empty', () => {
  let props = {
    ...initialProps,
    userLocation: {
      isLoading: false,
      coords: {},
      timestamp: moment().subtract(5, 'minutes')
    }
  }
  let wrapper = shallow(<App {...props} />)
  expect(wrapper.updateUserLocation).toBeCalled
})

it('updates user location on mount if timestamp is stale', () => {
  let props = {
    ...initialProps,
    userLocation: {
      isLoading: false,
      coords: { boop: 'beep' },
      timestamp: moment().subtract(12, 'minutes')
    }
  }
  let wrapper = shallow(<App {...props} />)
  expect(wrapper.updateUserLocation).toBeCalled
})

it('does not update user location if coords present and timestamp not stale', () => {
  let props = {
    ...initialProps,
    userLocation: {
      isLoading: false,
      coords: { boop: 'beep' },
      timestamp: moment().subtract(9, 'minutes')
    }
  }
  let wrapper = shallow(<App {...props} />)
  expect(wrapper.updateUserLocation).not.toBeCalled
})

it('updates bike stations on mount if data is empty', () => {
  let props = {
    ...initialProps,
    bikeStations: {
      isLoading: false,
      data: [],
      timestamp: moment().subtract(5, 'minutes')
    }
  }
  let wrapper = shallow(<App {...props} />)
  expect(wrapper.loadBikeStations).toBeCalled
})

it('updates bike stations on mount if timestamp is stale', () => {
  let props = {
    ...initialProps,
    bikeStations: {
      isLoading: false,
      data: [1, 2, 3],
      timestamp: moment().subtract(13, 'hours')
    }
  }
  let wrapper = shallow(<App {...props} />)
  expect(wrapper.loadBikeStations).toBeCalled
})

it('does not update user location if data present and timestamp not stale', () => {
  let props = {
    ...initialProps,
    bikeStations: {
      isLoading: false,
      data: [1, 2, 3],
      timestamp: moment().subtract(11, 'hours')
    }
  }
  let wrapper = shallow(<App {...props} />)
  expect(wrapper.updateUserLocation).not.toBeCalled
})
