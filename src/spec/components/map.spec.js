import React from 'react'
import { shallow } from 'enzyme'
import Map from '../../components/map'
import moment from 'moment'
import GoogleMapReact from 'google-map-react'
import MapStopMarker from '../../components/map_stop_marker'
import MapStopPreview from '../../components/map_stop_preview'

let initialProps = {
  coords: null,
  mapStopPreview: null,
  nearbyStops: { isLoading: true, data: [] },
  userLocation: { isLoading: true },
  loadNearbyStops: jest.fn(),
  toggleMapStopPreview: jest.fn(),
  updateMapCenter: jest.fn(),
  updateUserLocation: jest.fn()
}

it('updates user location on mount when coords not present', () => {
  let wrapper = shallow(<Map {...initialProps} />)
  expect(wrapper.updateUserLocation).toBeCalled
})

it('updates user location on mount when user location is stale', () => {
  let props = {
    ...initialProps,
    coords: { lat: 123, lon: 456 },
    userLocation: {
      timestamp: moment().subtract({ seconds: 61 }).valueOf()
    }
  }
  let wrapper = shallow(<Map {...props} />)
  expect(wrapper.updateUserLocation).toBeCalled
})

it('does not update user location when user location is not stale', () => {
  let props = {
    ...initialProps,
    coords: { lat: 123, lon: 456 },
    userLocation: {
      timestamp: moment().subtract({ seconds: 59 }).valueOf()
    }
  }
  let wrapper = shallow(<Map {...props} />)
  expect(wrapper.updateUserLocation).not.toBeCalled
})

it('renders the map correctly', () => {
  let props = {
    ...initialProps,
    coords: { lat: 123, lon: 456 },
    userLocation: { isLoading: false },
    nearbyStops: {
      isLoading: false,
      data: [{ stop_id: 1 }, { stop_id: 2 }, { stop_id: 3 }]
    }
  }
  let wrapper = shallow(<Map {...props} />)
  expect(wrapper.find('div.map-container')).toHaveLength(1)
  expect(wrapper.find('div.map-location-button')).toHaveLength(1)
  expect(wrapper.find(GoogleMapReact)).toHaveLength(1)
  expect(wrapper.find(MapStopMarker)).toHaveLength(3)
})

it('renders map stop preview when present', () => {
  let props = {
    ...initialProps,
    mapStopPreview: 1
  }
  let wrapper = shallow(<Map {...props} />)
  expect(wrapper.find(MapStopPreview)).toHaveLength(1)
})

it('removes map stop preview on unmount', () => {
  let props = {
    ...initialProps,
    mapStopPreview: 1
  }
  let wrapper = shallow(<Map {...props} />)
  wrapper.unmount()
  expect(wrapper.toggleMapStopPreview).toBeCalled
})
