import React from 'react'
import { shallow } from 'enzyme'
import Legend from '../../components/legend'

it('renders correctly', () => {
  let wrapper = shallow(<Legend />)
  expect(wrapper.find('div.legend')).toHaveLength(1)
  expect(wrapper.find('div.legend')).toIncludeText('Real-time')
  expect(wrapper.find('span.real-time')).toHaveLength(4)
  expect(wrapper.find('span.scheduled')).toHaveText('Scheduled')
  expect(wrapper.find('div.stop-arrival-chips')).toHaveLength(1)
  expect(wrapper.find('span.arrival-chip')).toHaveLength(3)
  expect(wrapper.find('span.arrival-chip.p5')).toHaveText('0-5 Min')
  expect(wrapper.find('span.arrival-chip.p10')).toHaveText('6-10 Min')
  expect(wrapper.find('span.arrival-chip.p20')).toHaveText('11+ Min')
})
