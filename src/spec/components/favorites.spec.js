import React from 'react'
import { shallow } from 'enzyme'
import Favorites from '../../components/favorites'
import StopPreview from '../../components/stop_preview'

let initialProps = {
  userFavorites: [],
  oldFavorites: null,
  convertFavorites: jest.fn()
}

it('renders correctly when favorites are present', () => {
  let props = {
    ...initialProps,
    userFavorites: [
      { stop_type: 'transit', stop_id: 1 },
      { stop_type: 'bike', stop_id: 2 }
    ]
  }
  let wrapper = shallow(<Favorites {...props} />)
  expect(wrapper.find('.heading')).toHaveText('Favorites')
  expect(wrapper.find(StopPreview)).toHaveLength(2)
})

it('renders correctly when no favorites present', () => {
  let wrapper = shallow(<Favorites {...initialProps} />)
  expect(wrapper.find('.heading')).toHaveText('Favorites')
  expect(wrapper.find('.message')).toIncludeText(
    "You don't have any favorites yet!"
  )
})

it('returns null when favorites present but in wrong format', () => {
  let props = { userFavorites: [{ boop: 'beep' }] }
  let wrapper = shallow(<Favorites {...props} />)
  expect(wrapper).toHaveText('Favorites')
  expect(wrapper.find(StopPreview)).toHaveLength(0)
  expect(wrapper.find('.message')).toHaveLength(0)
})

it('converts favorites on mount when oldFavorites are present', () => {
  let props = {
    ...initialProps,
    oldFavorites: [1, 2, 3, 4, 5, 6]
  }
  let wrapper = shallow(<Favorites {...props} />)
  expect(wrapper.convertFavorites).toBeCalled
})

it('does not convert favorites when oldFavorites are empty', () => {
  let props = {
    ...initialProps,
    oldFavorites: []
  }
  let wrapper = shallow(<Favorites {...props} />)
  expect(wrapper.convertFavorites).not.toBeCalled
})

it('does not convert favorites when oldFavorites are null', () => {
  let props = {
    ...initialProps,
    oldFavorites: null
  }
  let wrapper = shallow(<Favorites {...props} />)
  expect(wrapper.convertFavorites).not.toBeCalled
})
