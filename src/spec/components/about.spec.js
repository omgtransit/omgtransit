import React from 'react'
import { shallow } from 'enzyme'
import About from '../../components/about'

it('renders correctly', () => {
  let wrapper = shallow(<About />)
  expect(wrapper.find('div.heading')).toHaveText('About')
  expect(wrapper.find('div.static-content')).toHaveLength(1)
  expect(wrapper.find('div.team')).toHaveLength(1)
  expect(wrapper.find('img')).toHaveLength(6)
  expect(wrapper.find('ul.awards')).toHaveLength(1)
  expect(wrapper.find('ul.awards li')).toHaveLength(5)
})
