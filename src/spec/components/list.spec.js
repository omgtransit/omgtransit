import React from 'react'
import { shallow } from 'enzyme'
import List from '../../components/list'
import StopPreview from '../../components/stop_preview'
import Legend from '../../components/legend'

let initialProps = {
  userLocation: { isLoading: true },
  nearbyStops: { isLoading: true },
  transitArrivals: { isLoading: true },
  updateUserLocation: jest.fn(),
  loadNearbyStops: jest.fn(),
  updateMapCenter: jest.fn()
}

describe('loading', () => {
  it('renders correctly while loading location', () => {
    let wrapper = shallow(<List {...initialProps} />)
    expect(wrapper.find('div.message')).toHaveText('Loading your location...')
    expect(wrapper.updateUserLocation).toBeCalled
  })

  it('renders correctly while loading nearby stops', () => {
    let props = {
      ...initialProps,
      userLocation: { isLoading: false }
    }
    let wrapper = shallow(<List {...props} />)
    expect(wrapper.find('div.message')).toHaveText('Loading nearby stops...')
    expect(wrapper.loadNearbyStops).toBeCalled
  })

  it('does not update user location when coords are present', () => {
    let props = {
      ...initialProps,
      userLocation: { isLoading: false },
      coords: { lat: 123, lon: 456 }
    }
    let wrapper = shallow(<List {...props} />)
    expect(wrapper.updateUserLocation).not.toBeCalled
  })
})

describe('errors', () => {
  it('renders correctly when location access is not granted', () => {
    let props = {
      ...initialProps,
      userLocation: { isLoading: false, error: 'boop' }
    }
    let wrapper = shallow(<List {...props} />)
    expect(wrapper.find('span.heading')).toIncludeText('Doh!')
    expect(wrapper.find('div.message')).toIncludeText(
      'OMG Transit relies on your geolocation to show nearby transit options.'
    )
    expect(wrapper.find('div.message')).toIncludeText(
      'Please allow location access to get the most out of our app!'
    )
    expect(wrapper.find('div.message')).toIncludeText(
      'Click here to go straight to Downtown Minneapolis instead.'
    )
    expect(wrapper.find(Legend)).toHaveLength(0)

    wrapper.find('a').simulate('click', { preventDefault() {} })
    expect(wrapper.updateMapCenter).toBeCalled
  })

  it('renders correctly when loading arrival data fails', () => {
    let props = {
      ...initialProps,
      userLocation: { isLoading: false },
      nearbyStops: { isLoading: false, data: [{ stop_id: 1 }] },
      transitArrivals: { isLoading: false, error: 'beep' }
    }
    let wrapper = shallow(<List {...props} />)
    expect(wrapper.find('span.heading')).toIncludeText('Network error!')
    expect(wrapper.find('div.message')).toIncludeText(
      'Something prevented us from loading real-time data for your nearby stops.'
    )
    expect(wrapper.find('div.message')).toIncludeText(
      'If you have an ad blocker like Privacy Badger enabled, please disable it for this page.'
    )
    expect(wrapper.find(Legend)).toHaveLength(0)
  })

  it('renders correctly when no nearby stops present', () => {
    let props = {
      ...initialProps,
      userLocation: { isLoading: false },
      nearbyStops: { isLoading: false, data: [] }
    }

    let wrapper = shallow(<List {...props} />)
    expect(wrapper.loadNearbyStops).toBeCalled
    expect(wrapper.find('span.heading')).toIncludeText('Zoinks!')
    expect(wrapper.find('div.message')).toIncludeText(
      "We couldn't find any stops near your current location."
    )
    expect(wrapper.find('div.message')).toIncludeText(
      'Click here to see Downtown Minneapolis instead.'
    )
    expect(wrapper.find(Legend)).toHaveLength(0)

    wrapper.find('a').simulate('click', { preventDefault() {} })
    expect(wrapper.updateMapCenter).toBeCalled
  })
})

it('renders correctly when nearby stops are present', () => {
  let props = {
    ...initialProps,
    userLocation: { isLoading: false },
    nearbyStops: {
      isLoading: false,
      data: [{ stop_id: 1 }, { stop_id: 2 }, { stop_id: 3 }]
    }
  }

  let wrapper = shallow(<List {...props} />)
  expect(wrapper.find(StopPreview)).toHaveLength(3)
  expect(wrapper.find(Legend)).toHaveLength(1)
})

it('only loads nearby stops when coords change', () => {
  let props = {
    ...initialProps,
    userLocation: { isLoading: false },
    nearbyStops: { isLoading: false, data: [{ stop_id: 1 }] },
    coords: { lat: 123, lon: 456 }
  }

  let wrapper = shallow(<List {...props} />)
  expect(wrapper.find(StopPreview)).toHaveLength(1)
  expect(wrapper.find(Legend)).toHaveLength(1)
  expect(wrapper.loadNearbyStops).not.toBeCalled

  // coords did not change
  wrapper.setProps({ coords: { lat: 123, lon: 456 } })
  expect(wrapper.loadNearbyStops).not.toBeCalled

  // coords changed
  wrapper.setProps({ coords: { lat: 456, lon: 789 } })
  expect(wrapper.loadNearbyStops).toBeCalled
})
