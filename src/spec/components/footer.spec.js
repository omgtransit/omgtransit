import React from 'react'
import { shallow } from 'enzyme'
import Footer from '../../components/footer'
import { NavLink } from 'react-router-dom'

import * as helpers from '../../util/helpers'

it('renders correctly', () => {
  let wrapper = shallow(<Footer />)
  expect(wrapper.find('#footer')).toHaveLength(1)
  expect(wrapper.find('#footer')).not.toHaveClassName('iphonex')
  expect(wrapper.find(NavLink)).toHaveLength(3)
  expect(wrapper.find('span.text')).toHaveLength(3)
})
